package com.example.testmvvm;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.animation.DecelerateInterpolator;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class AnimUtil {
    private List<ValueAnimator> mPulseEffectAnimatorList1;
    private List<ValueAnimator> mPulseEffectAnimatorList2;
    private List<ValueAnimator> mPulseEffectAnimatorList3;
    private List<ValueAnimator> mPulseEffectAnimatorList4;
    private List<ValueAnimator> mPulseEffectAnimatorList5;
    private List<ValueAnimator> mPulseEffectAnimatorList6;
    private List<ValueAnimator> mPulseEffectAnimatorList7;
    private List<ValueAnimator> mPulseEffectAnimatorList8;

    Context context;
    public AnimUtil(Context context) {
        this.context = context;
    }


    public void initPulseEffect(GoogleMap mMap, LatLng currentPos, int pulseColor, float zoom) {
        if (mPulseEffectAnimatorList1!=null)
            mPulseEffectAnimatorList1.clear();
        else
            mPulseEffectAnimatorList1 = new ArrayList<>();

        if (mPulseEffectAnimatorList2!=null)
            mPulseEffectAnimatorList2.clear();
        else
            mPulseEffectAnimatorList2 = new ArrayList<>();

        if (mPulseEffectAnimatorList3!=null)
            mPulseEffectAnimatorList3.clear();
        else
            mPulseEffectAnimatorList3 = new ArrayList<>();

        if (mPulseEffectAnimatorList4!=null)
            mPulseEffectAnimatorList4.clear();
        else
            mPulseEffectAnimatorList4 = new ArrayList<>();

        if (mPulseEffectAnimatorList5!=null)
            mPulseEffectAnimatorList5.clear();
        else
            mPulseEffectAnimatorList5 = new ArrayList<>();

        if (mPulseEffectAnimatorList6!=null)
            mPulseEffectAnimatorList6.clear();
        else
            mPulseEffectAnimatorList6 = new ArrayList<>();

        if (mPulseEffectAnimatorList7!=null)
            mPulseEffectAnimatorList7.clear();
        else
            mPulseEffectAnimatorList7 = new ArrayList<>();

        if (mPulseEffectAnimatorList8!=null)
            mPulseEffectAnimatorList8.clear();
        else
            mPulseEffectAnimatorList8 = new ArrayList<>();


        @ColorInt int mPulseEffectColor1;
        @ColorInt int mPulseEffectColor2;
        @ColorInt int mPulseEffectColor3;
        @ColorInt int mPulseEffectColor4;
        @ColorInt int mPulseEffectColor5;
        @ColorInt int mPulseEffectColor6;
        @ColorInt int mPulseEffectColor7;
        @ColorInt int mPulseEffectColor8;

        final int[] mPulseEffectColorElements1;
        final int[] mPulseEffectColorElements2;
        final int[] mPulseEffectColorElements3;
        final int[] mPulseEffectColorElements4;
        final int[] mPulseEffectColorElements5;
        final int[] mPulseEffectColorElements6;
        final int[] mPulseEffectColorElements7;
        final int[] mPulseEffectColorElements8;

        final ValueAnimator mPulseEffectAnimator1;
        final ValueAnimator mPulseEffectAnimator2;
        final ValueAnimator mPulseEffectAnimator3;
        final ValueAnimator mPulseEffectAnimator4;
        final ValueAnimator mPulseEffectAnimator5;
        final ValueAnimator mPulseEffectAnimator6;
        final ValueAnimator mPulseEffectAnimator7;
        final ValueAnimator mPulseEffectAnimator8;

        Circle mPulseCircle1 = null;
        Circle mPulseCircle2 = null;
        Circle mPulseCircle3 = null;
        Circle mPulseCircle4 = null;
        Circle mPulseCircle5 = null;
        Circle mPulseCircle6 = null;
        Circle mPulseCircle7 = null;
        Circle mPulseCircle8 = null;

        mPulseEffectColor1 = ContextCompat.getColor(context, R.color.pulseColorWhite);
        mPulseEffectColorElements1 = new int[] {
                Color.red(mPulseEffectColor1),
                Color.green(mPulseEffectColor1),
                Color.blue(mPulseEffectColor1)
        };

        mPulseEffectColor2 = ContextCompat.getColor(context, pulseColor);
        mPulseEffectColorElements2 = new int[] {
                Color.red(mPulseEffectColor2),
                Color.green(mPulseEffectColor2),
                Color.blue(mPulseEffectColor2)
        };

        mPulseEffectColor3 = ContextCompat.getColor(context, R.color.pulseColorWhite);
        mPulseEffectColorElements3 = new int[] {
                Color.red(mPulseEffectColor3),
                Color.green(mPulseEffectColor3),
                Color.blue(mPulseEffectColor3)
        };

        mPulseEffectColor4 = ContextCompat.getColor(context, pulseColor);
        mPulseEffectColorElements4 = new int[] {
                Color.red(mPulseEffectColor4),
                Color.green(mPulseEffectColor4),
                Color.blue(mPulseEffectColor4)
        };

        mPulseEffectColor5 = ContextCompat.getColor(context, R.color.pulseColorWhite);
        mPulseEffectColorElements5 = new int[] {
                Color.red(mPulseEffectColor5),
                Color.green(mPulseEffectColor5),
                Color.blue(mPulseEffectColor5)
        };

        mPulseEffectColor6 = ContextCompat.getColor(context, pulseColor);
        mPulseEffectColorElements6 = new int[] {
                Color.red(mPulseEffectColor6),
                Color.green(mPulseEffectColor6),
                Color.blue(mPulseEffectColor6)
        };

        mPulseEffectColor7 = ContextCompat.getColor(context, R.color.pulseColorWhite);
        mPulseEffectColorElements7 = new int[] {
                Color.red(mPulseEffectColor7),
                Color.green(mPulseEffectColor7),
                Color.blue(mPulseEffectColor7)
        };

        mPulseEffectColor8 = ContextCompat.getColor(context, pulseColor);
        mPulseEffectColorElements8 = new int[] {
                Color.red(mPulseEffectColor8),
                Color.green(mPulseEffectColor8),
                Color.blue(mPulseEffectColor8)
        };

        mPulseEffectAnimator1 = ValueAnimator.ofFloat(0, calculatePulseRadius(zoom));
        mPulseEffectAnimator1.setStartDelay(0);
        mPulseEffectAnimator1.setDuration(3200);
        mPulseEffectAnimator1.setInterpolator(new DecelerateInterpolator(2));

        mPulseEffectAnimator2 = ValueAnimator.ofFloat(0, calculatePulseRadius(zoom));
        mPulseEffectAnimator2.setStartDelay(0);
        mPulseEffectAnimator2.setDuration(3200);
        mPulseEffectAnimator2.setInterpolator(new DecelerateInterpolator(2));

        mPulseEffectAnimator4 = ValueAnimator.ofFloat(0, calculatePulseRadius(zoom));
        mPulseEffectAnimator4.setStartDelay(0);
        mPulseEffectAnimator4.setDuration(3200);
        mPulseEffectAnimator4.setInterpolator(new DecelerateInterpolator(2));

        mPulseEffectAnimator3 = ValueAnimator.ofFloat(0, calculatePulseRadius(zoom));
        mPulseEffectAnimator3.setStartDelay(0);
        mPulseEffectAnimator3.setDuration(3200);
        mPulseEffectAnimator3.setInterpolator(new DecelerateInterpolator(2));

        mPulseEffectAnimator5 = ValueAnimator.ofFloat(0, calculatePulseRadius(zoom));
        mPulseEffectAnimator5.setStartDelay(0);
        mPulseEffectAnimator5.setDuration(3200);
        mPulseEffectAnimator5.setInterpolator(new DecelerateInterpolator(2));

        mPulseEffectAnimator6 = ValueAnimator.ofFloat(0, calculatePulseRadius(zoom));
        mPulseEffectAnimator6.setStartDelay(0);
        mPulseEffectAnimator6.setDuration(3200);
        mPulseEffectAnimator6.setInterpolator(new DecelerateInterpolator(2));

        mPulseEffectAnimator7 = ValueAnimator.ofFloat(0, calculatePulseRadius(zoom));
        mPulseEffectAnimator7.setStartDelay(0);
        mPulseEffectAnimator7.setDuration(3200);
        mPulseEffectAnimator7.setInterpolator(new DecelerateInterpolator(2));

        mPulseEffectAnimator8 = ValueAnimator.ofFloat(0, calculatePulseRadius(zoom));
        mPulseEffectAnimator8.setStartDelay(0);
        mPulseEffectAnimator8.setDuration(3200);
        mPulseEffectAnimator8.setInterpolator(new DecelerateInterpolator(2));

        /////////////////////////////////////////////

        if (mPulseCircle1 != null) {
            mPulseCircle1.remove();
            if (mPulseCircle2 !=null){
                mPulseCircle2.remove();
                if (mPulseCircle3 !=null){
                    mPulseCircle3.remove();
                    if (mPulseCircle4 !=null){
                        mPulseCircle4.remove();
                        if (mPulseCircle5 !=null){
                            mPulseCircle5.remove();
                            if (mPulseCircle6 !=null){
                                mPulseCircle6.remove();
                                if (mPulseCircle7 !=null){
                                    mPulseCircle7.remove();
                                    if (mPulseCircle8 !=null){
                                        mPulseCircle8.remove();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (mPulseEffectAnimator1 != null) {
            mPulseEffectAnimator1.removeAllUpdateListeners();
            mPulseEffectAnimator1.removeAllListeners();
            mPulseEffectAnimator1.end();
        }

        if (mPulseEffectAnimator2 != null) {
            mPulseEffectAnimator2.removeAllUpdateListeners();
            mPulseEffectAnimator2.removeAllListeners();
            mPulseEffectAnimator2.end();
        }

        if (mPulseEffectAnimator4 != null) {
            mPulseEffectAnimator4.removeAllUpdateListeners();
            mPulseEffectAnimator4.removeAllListeners();
            mPulseEffectAnimator4.end();
        }

        if (mPulseEffectAnimator3 != null) {
            mPulseEffectAnimator3.removeAllUpdateListeners();
            mPulseEffectAnimator3.removeAllListeners();
            mPulseEffectAnimator3.end();
        }

        if (mPulseEffectAnimator5 != null) {
            mPulseEffectAnimator5.removeAllUpdateListeners();
            mPulseEffectAnimator5.removeAllListeners();
            mPulseEffectAnimator5.end();
        }

        if (mPulseEffectAnimator6 != null) {
            mPulseEffectAnimator6.removeAllUpdateListeners();
            mPulseEffectAnimator6.removeAllListeners();
            mPulseEffectAnimator6.end();
        }

        if (mPulseEffectAnimator7 != null) {
            mPulseEffectAnimator7.removeAllUpdateListeners();
            mPulseEffectAnimator7.removeAllListeners();
            mPulseEffectAnimator7.end();
        }

        if (mPulseEffectAnimator8 != null) {
            mPulseEffectAnimator8.removeAllUpdateListeners();
            mPulseEffectAnimator8.removeAllListeners();
            mPulseEffectAnimator8.end();
        }


        if (currentPos != null) {

            mPulseCircle1 = mMap.addCircle(new CircleOptions()
                    .center(currentPos)
                    .radius(0)
                    .strokeWidth(0)
                    .fillColor(mPulseEffectColor1));

            mPulseCircle2 = mMap.addCircle(new CircleOptions()
                    .center(currentPos)
                    .radius(0)
                    .strokeWidth(0)
                    .fillColor(mPulseEffectColor2));

            mPulseCircle3 = mMap.addCircle(new CircleOptions()
                    .center(currentPos)
                    .radius(0)
                    .strokeWidth(0)
                    .fillColor(mPulseEffectColor3));

            mPulseCircle4 = mMap.addCircle(new CircleOptions()
                    .center(currentPos)
                    .radius(0)
                    .strokeWidth(0)
                    .fillColor(mPulseEffectColor4));

            mPulseCircle5 = mMap.addCircle(new CircleOptions()
                    .center(currentPos)
                    .radius(0).strokeWidth(0)
                    .fillColor(mPulseEffectColor5));

            mPulseCircle6 = mMap.addCircle(new CircleOptions()
                    .center(currentPos)
                    .radius(0).strokeWidth(0)
                    .fillColor(mPulseEffectColor6));

            mPulseCircle7 = mMap.addCircle(new CircleOptions()
                    .center(currentPos)
                    .radius(0).strokeWidth(0)
                    .fillColor(mPulseEffectColor7));

            mPulseCircle8 = mMap.addCircle(new CircleOptions()
                    .center(currentPos)
                    .radius(0).strokeWidth(0)
                    .fillColor(mPulseEffectColor8));

            final Circle finalMPulseCircle1 = mPulseCircle1;
            final Circle finalMPulseCircle2 = mPulseCircle2;
            final Circle finalMPulseCircle3 = mPulseCircle3;
            final Circle finalMPulseCircle4 = mPulseCircle4;
            final Circle finalMPulseCircle5 = mPulseCircle5;
            final Circle finalMPulseCircle6 = mPulseCircle6;
            final Circle finalMPulseCircle7 = mPulseCircle7;
            final Circle finalMPulseCircle8 = mPulseCircle8;

            mPulseEffectAnimator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (finalMPulseCircle1 == null)
                        return;

                    int alpha1 = (int) ((1 - valueAnimator.getAnimatedFraction()) * 255 *2);
                    Log.e("ALPHA", "ALPHA 1: "+alpha1);
                    finalMPulseCircle1.setFillColor(Color.argb(alpha1,
                            mPulseEffectColorElements1[0], mPulseEffectColorElements1[1], mPulseEffectColorElements1[2]));
                    finalMPulseCircle1.setRadius((float) valueAnimator.getAnimatedValue());
                }
            });
            mPulseEffectAnimator1.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mPulseEffectAnimator1.setStartDelay(0);
                    mPulseEffectAnimator1.start();
                }
            });


            mPulseEffectAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (finalMPulseCircle2 == null)
                        return;

                    int alpha2 = (int) ((1 - valueAnimator.getAnimatedFraction()) * 255 *2);
                    finalMPulseCircle2.setFillColor(Color.argb(alpha2,
                            mPulseEffectColorElements2[0], mPulseEffectColorElements2[1], mPulseEffectColorElements2[2]));
                    finalMPulseCircle2.setRadius((float) valueAnimator.getAnimatedValue());
                }
            });
            mPulseEffectAnimator2.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mPulseEffectAnimator2.setStartDelay(0);
                    mPulseEffectAnimator2.start();
                }
            });

            mPulseEffectAnimator3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (finalMPulseCircle3 == null)
                        return;

                    int alpha1 = (int) ((1 - valueAnimator.getAnimatedFraction()) * 255 *2);
                    finalMPulseCircle3.setFillColor(Color.argb(alpha1,
                            mPulseEffectColorElements3[0], mPulseEffectColorElements3[1], mPulseEffectColorElements3[2]));
                    finalMPulseCircle3.setRadius((float) valueAnimator.getAnimatedValue());
                }
            });
            mPulseEffectAnimator3.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mPulseEffectAnimator3.setStartDelay(0);
                    mPulseEffectAnimator3.start();
                }
            });

            mPulseEffectAnimator4.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (finalMPulseCircle4 == null)
                        return;

                    int alpha1 = (int) ((1 - valueAnimator.getAnimatedFraction()) * 255 *2);
                    finalMPulseCircle4.setFillColor(Color.argb(alpha1,
                            mPulseEffectColorElements4[0], mPulseEffectColorElements4[1], mPulseEffectColorElements4[2]));
                    finalMPulseCircle4.setRadius((float) valueAnimator.getAnimatedValue());
                }
            });
            mPulseEffectAnimator4.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mPulseEffectAnimator4.setStartDelay(0);
                    mPulseEffectAnimator4.start();
                }
            });

            mPulseEffectAnimator5.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (finalMPulseCircle5 == null)
                        return;

                    int alpha5 = (int) ((1 - valueAnimator.getAnimatedFraction()) * 255 *2);
                    finalMPulseCircle5.setFillColor(Color.argb(alpha5,
                            mPulseEffectColorElements5[0], mPulseEffectColorElements5[1], mPulseEffectColorElements5[2]));
                    finalMPulseCircle5.setRadius((float) valueAnimator.getAnimatedValue());
                }
            });
            mPulseEffectAnimator5.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mPulseEffectAnimator5.setStartDelay(0);
                    mPulseEffectAnimator5.start();
                }
            });

            mPulseEffectAnimator6.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (finalMPulseCircle6 == null)
                        return;

                    int alpha6 = (int) ((1 - valueAnimator.getAnimatedFraction()) * 255 *2);
                    finalMPulseCircle6.setFillColor(Color.argb(alpha6,
                            mPulseEffectColorElements6[0], mPulseEffectColorElements6[1], mPulseEffectColorElements6[2]));
                    finalMPulseCircle6.setRadius((float) valueAnimator.getAnimatedValue());
                }
            });
            mPulseEffectAnimator6.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mPulseEffectAnimator6.setStartDelay(0);
                    mPulseEffectAnimator6.start();
                }
            });

            mPulseEffectAnimator7.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (finalMPulseCircle7 == null)
                        return;

                    int alpha7 = (int) ((1 - valueAnimator.getAnimatedFraction()) * 255 *2);
                    finalMPulseCircle7.setFillColor(Color.argb(alpha7,
                            mPulseEffectColorElements7[0], mPulseEffectColorElements7[1], mPulseEffectColorElements7[2]));
                    finalMPulseCircle7.setRadius((float) valueAnimator.getAnimatedValue());
                }
            });
            mPulseEffectAnimator7.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mPulseEffectAnimator7.setStartDelay(0);
                    mPulseEffectAnimator7.start();
                }
            });

            mPulseEffectAnimator8.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (finalMPulseCircle8 == null)
                        return;

                    int alpha8 = (int) ((1 - valueAnimator.getAnimatedFraction()) * 255 *2);
                    finalMPulseCircle8.setFillColor(Color.argb(alpha8,
                            mPulseEffectColorElements8[0], mPulseEffectColorElements8[1], mPulseEffectColorElements8[2]));
                    finalMPulseCircle8.setRadius((float) valueAnimator.getAnimatedValue());
                }
            });
            mPulseEffectAnimator8.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mPulseEffectAnimator8.setStartDelay(0);
                    mPulseEffectAnimator8.start();
                }
            });

            mPulseEffectAnimator1.start();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPulseEffectAnimator2.start();
                    mPulseEffectAnimatorList2.add(mPulseEffectAnimator2);
                }
            },400);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPulseEffectAnimator3.start();
                    mPulseEffectAnimatorList3.add(mPulseEffectAnimator3);
                }
            },800);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPulseEffectAnimator4.start();
                    mPulseEffectAnimatorList4.add(mPulseEffectAnimator4);
                }
            },1200);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPulseEffectAnimator5.start();
                    mPulseEffectAnimatorList5.add(mPulseEffectAnimator5);
                }
            },1600);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPulseEffectAnimator6.start();
                    mPulseEffectAnimatorList6.add(mPulseEffectAnimator6);
                }
            },2000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPulseEffectAnimator7.start();
                    mPulseEffectAnimatorList7.add(mPulseEffectAnimator7);
                }
            },2400);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPulseEffectAnimator8.start();
                    mPulseEffectAnimatorList8.add(mPulseEffectAnimator8);
                }
            },2800);

            mPulseEffectAnimatorList1.add(mPulseEffectAnimator1);

        }
    }

    private static float calculatePulseRadius(float zoomLevel) {
        return (float) Math.pow(2, 16 - zoomLevel) * 75;
    }

    public void setRadius(CameraPosition cameraPosition){
        if (mPulseEffectAnimatorList1.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList1.size(); i++) {
                if (mPulseEffectAnimatorList1.get(i) != null)
                    mPulseEffectAnimatorList1.get(i).setFloatValues(0, calculatePulseRadius(cameraPosition.zoom));
            }
        }

        if (mPulseEffectAnimatorList2.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList2.size(); i++) {
                if (mPulseEffectAnimatorList2.get(i) != null)
                    mPulseEffectAnimatorList2.get(i).setFloatValues(0, calculatePulseRadius(cameraPosition.zoom));
            }
        }

        if (mPulseEffectAnimatorList3.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList3.size(); i++) {
                if (mPulseEffectAnimatorList3.get(i) != null)
                    mPulseEffectAnimatorList3.get(i).setFloatValues(0, calculatePulseRadius(cameraPosition.zoom));
            }
        }

        if (mPulseEffectAnimatorList4.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList4.size(); i++) {
                if (mPulseEffectAnimatorList4.get(i) != null)
                    mPulseEffectAnimatorList4.get(i).setFloatValues(0, calculatePulseRadius(cameraPosition.zoom));
            }
        }

        if (mPulseEffectAnimatorList5.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList5.size(); i++) {
                if (mPulseEffectAnimatorList5.get(i) != null)
                    mPulseEffectAnimatorList5.get(i).setFloatValues(0, calculatePulseRadius(cameraPosition.zoom));
            }
        }

        if (mPulseEffectAnimatorList6.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList6.size(); i++) {
                if (mPulseEffectAnimatorList6.get(i) != null)
                    mPulseEffectAnimatorList6.get(i).setFloatValues(0, calculatePulseRadius(cameraPosition.zoom));
            }
        }

        if (mPulseEffectAnimatorList7.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList7.size(); i++) {
                if (mPulseEffectAnimatorList7.get(i) != null)
                    mPulseEffectAnimatorList7.get(i).setFloatValues(0, calculatePulseRadius(cameraPosition.zoom));
            }
        }

        if (mPulseEffectAnimatorList8.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList8.size(); i++) {
                if (mPulseEffectAnimatorList8.get(i) != null)
                    mPulseEffectAnimatorList8.get(i).setFloatValues(0, calculatePulseRadius(cameraPosition.zoom));
            }
        }
    }

    public void closeAnimation(){
        if (mPulseEffectAnimatorList1.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList1.size(); i++) {
                if (mPulseEffectAnimatorList1.get(i) != null){
                    if(mPulseEffectAnimatorList1.get(i).isRunning()){
                        mPulseEffectAnimatorList1.get(i).removeAllListeners();
                        mPulseEffectAnimatorList1.get(i).end();
                    }
                }
            }
        }

        if (mPulseEffectAnimatorList2.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList2.size(); i++) {
                if (mPulseEffectAnimatorList2.get(i) != null){
                    if(mPulseEffectAnimatorList2.get(i).isRunning()){
                        mPulseEffectAnimatorList2.get(i).removeAllListeners();
                        mPulseEffectAnimatorList2.get(i).end();
                    }
                }
            }
        }

        if (mPulseEffectAnimatorList3.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList3.size(); i++) {
                if (mPulseEffectAnimatorList3.get(i) != null) {
                    if(mPulseEffectAnimatorList3.get(i).isRunning()){
                        mPulseEffectAnimatorList3.get(i).removeAllListeners();
                        mPulseEffectAnimatorList3.get(i).end();
                    }
                }
            }
        }

        if (mPulseEffectAnimatorList4.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList4.size(); i++) {
                if (mPulseEffectAnimatorList4.get(i) != null) {
                    if(mPulseEffectAnimatorList4.get(i).isRunning()){
                        mPulseEffectAnimatorList4.get(i).removeAllListeners();
                        mPulseEffectAnimatorList4.get(i).end();
                    }
                }
            }
        }

        if (mPulseEffectAnimatorList5.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList5.size(); i++) {
                if (mPulseEffectAnimatorList5.get(i) != null){
                    if(mPulseEffectAnimatorList5.get(i).isRunning()){
                        mPulseEffectAnimatorList5.get(i).removeAllListeners();
                        mPulseEffectAnimatorList5.get(i).end();
                    }
                }
            }
        }

        if (mPulseEffectAnimatorList6.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList6.size(); i++) {
                if (mPulseEffectAnimatorList6.get(i) != null){
                    if(mPulseEffectAnimatorList6.get(i).isRunning()){
                        mPulseEffectAnimatorList6.get(i).removeAllListeners();
                        mPulseEffectAnimatorList6.get(i).end();
                    }
                }
            }
        }

        if (mPulseEffectAnimatorList7.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList7.size(); i++) {
                if (mPulseEffectAnimatorList7.get(i) != null){
                    if(mPulseEffectAnimatorList7.get(i).isRunning()){
                        mPulseEffectAnimatorList7.get(i).removeAllListeners();
                        mPulseEffectAnimatorList7.get(i).end();
                    }
                }
            }
        }

        if (mPulseEffectAnimatorList8.size()>0) {
            for (int i = 0; i < mPulseEffectAnimatorList8.size(); i++) {
                if (mPulseEffectAnimatorList8.get(i) != null){
                    if(mPulseEffectAnimatorList8.get(i).isRunning()){
                        mPulseEffectAnimatorList8.get(i).removeAllListeners();
                        mPulseEffectAnimatorList8.get(i).end();
                    }
                }
            }
        }
    }
}
