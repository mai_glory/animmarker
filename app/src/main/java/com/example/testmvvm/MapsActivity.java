package com.example.testmvvm;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private GoogleMap mMap;
    private AnimUtil animUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        animUtil = new AnimUtil(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    LatLng sydney;
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraIdleListener(this);
        // Add a marker in Sydney and move the camera
        sydney = new LatLng(-34, 151);
        LatLng mlbrn = new LatLng( -37.840935, 144.946457);
        LatLng townsville = new LatLng(-19.258965, 146.816956);
        LatLng brisben = new LatLng(-27.470125, 153.021072);
        LatLng sandover = new LatLng(-21.7166638, 136.5333312);
        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);

        Marker sydneyMarker = mMap.addMarker(new MarkerOptions()
                .position(sydney)
                .title("Title")
                .snippet("Description")
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))));
        sydneyMarker.setAnchor(0.5f, 0.5f);

        /*Marker mlbrnMarker = mMap.addMarker(new MarkerOptions()
                .position(mlbrn)
                .title("Title")
                .snippet("Description")
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))));
        mlbrnMarker.setAnchor(0.5f, 0.5f);

        Marker townsvilleMarker = mMap.addMarker(new MarkerOptions()
                .position(townsville)
                .title("Title")
                .snippet("Description")
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))));
        townsvilleMarker.setAnchor(0.5f, 0.5f);

        Marker brisbenMarker = mMap.addMarker(new MarkerOptions()
                .position(brisben)
                .title("Title")
                .snippet("Description")
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))));
        brisbenMarker.setAnchor(0.5f, 0.5f);

        Marker sandoverMarker = mMap.addMarker(new MarkerOptions()
                .position(sandover)
                .title("Title")
                .snippet("Description")
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))));
        sandoverMarker.setAnchor(0.5f, 0.5f);*/
        /*mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));*/
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        animUtil.initPulseEffect(mMap, sydney, R.color.pulseColorTeal, mMap.getCameraPosition().zoom);
        /*initPulseEffect(mlbrn);
        initPulseEffect(townsville);
        initPulseEffect(brisben);
        initPulseEffect(sandover);*/
        //startPulseAnimation(mlbrn);
    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    @Override
    public void onCameraIdle() {
        CameraPosition cameraPosition = mMap.getCameraPosition();
        animUtil.setRadius(cameraPosition);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        animUtil.closeAnimation();
    }

    public void onColorClick(View view) {
        switch (view.getId()){
            case R.id.btnTeal:
                if (sydney!=null){
                    animUtil.closeAnimation();
                    setClickEnable(view);
                    selectClicked(view);
                    animUtil.initPulseEffect(mMap, sydney, R.color.pulseColorTeal, mMap.getCameraPosition().zoom);
                }
                break;

            case R.id.btnPurple:
                if (sydney!=null){
                    animUtil.closeAnimation();
                    setClickEnable(view);
                    selectClicked(view);
                    animUtil.initPulseEffect(mMap, sydney, R.color.pulseColorPurple, mMap.getCameraPosition().zoom);
                }
                break;

            case R.id.btnRed:
                if (sydney!=null){
                    animUtil.closeAnimation();
                    setClickEnable(view);
                    selectClicked(view);
                    animUtil.initPulseEffect(mMap, sydney, R.color.pulseColorRed, mMap.getCameraPosition().zoom);
                }
                break;
        }
    }

    private void setClickEnable(View view) {
        findViewById(R.id.btnTeal).setClickable(true);
        findViewById(R.id.btnPurple).setClickable(true);
        findViewById(R.id.btnRed).setClickable(true);
        view.setClickable(false);
    }

    private void selectClicked(View view) {
        findViewById(R.id.btnTeal).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        findViewById(R.id.btnPurple).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        findViewById(R.id.btnRed).setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        view.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
    }
}
