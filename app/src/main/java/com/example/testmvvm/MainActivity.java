package com.example.testmvvm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView imageView = (ImageView) findViewById(R.id.image);
        imageView.setClipToOutline(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(v);
            }
        });

        /*CircleImageView circleImageView = (CircleImageView) findViewById(R.id.c_image);
        circleImageView.setBorderWidth(4);
        circleImageView.setBorderColor(Color.parseColor("#ffffff"));


        final View view = (View) findViewById(R.id.image);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.scale_alpha_anim);
        view.setAnimation(animation);*/
        /*view.animate().alpha(0.1f).scaleX(2).scaleY(2).setDuration(3000).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();*/
    }

    public void click(View view) {
        startActivity(new Intent(this, MapsActivity.class));
    }
}
